Handy tool to try out HTML5 and CSS3 code snippets: [http://www.cssdesk.com/](http://www.cssdesk.com/)

If you have any questions? Email contact@codinggrace.com

## Slides

* [Part 1](http://bit.ly/1Gx2RIE) - setup and intro to HTML5 and CSS3

* [Part 2](http://bit.ly/1Hnhtt5) - More about HTML5 and CSS3
* [Part 3](http://bit.ly/1f5y0XH) - Debugging, layouts and positioning, and final exercise to create a cv/profile page 

## Exercises

Aside from the exercises here, you can also find Part 3 examples (blog, normalise.css, blog post, floats and positioning) at [http://stephaniefrancis.github.io/codinggrace/](http://stephaniefrancis.github.io/codinggrace/)

### Exercise1
Basic tags and structure of a HTML document.

### Exercise2
More HTML and introducing CSS and how to link to HTML document.

### Exercise3
Semantic HTML5, CSS design and linking to Google Fonts.

### ExerciseFinal
Create your own resume/cv web page.

* **resume-text.txt** - contains sample cv/resume details to include in a resume/cv HTML page
* **SameResumeTemplate** - folder contains sample HTML and CSS code for a resume page for your reference. (Template thanks to [http://sampleresumetemplate.net](http://sampleresumetemplate.net))

## References

### HTML5 Resources
* [http://www.html5rocks.com](http://www.html5rocks.com)
* [http://html5doctor.com/](http://html5doctor.com/) 
* [http://reference.sitepoint.com/html](http://reference.sitepoint.com/html) 
* [http://woorkup.com/2009/12/16/html5-visual-cheat-sheet-reloaded/](http://woorkup.com/2009/12/16/html5-visual-cheat-sheet-reloaded/) 

### CSS3
* [http://css3please.com](http://css3please.com)
* [http://css-tricks.com/almanac/](http://css-tricks.com/almanac/)
* [http://csszengarden.com](http://csszengarden.com)
* [http://realworldcss3.com/resources/](http://realworldcss3.com/resources/)
* [http://reference.sitepoint.com/css](http://reference.sitepoint.com/css)
* [http://simplilearn.com/css3-resources-ultimate-list-article](http://simplilearn.com/css3-resources-ultimate-list-article)

### Lorem Ipsum Generators / Generic Placeholders
* [http://www.lipsum.com/](http://www.lipsum.com/)
* [http://www.catipsum.com/](http://www.catipsum.com/)
* [http://robohash.org/](http://robohash.org/)
* [http://placekitten.com/](http://placekitten.com/)
